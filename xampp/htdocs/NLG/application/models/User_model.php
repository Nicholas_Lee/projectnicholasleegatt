<?php
    class User_model extends CI_Model{
        
         private $id;
        
        
        function __construct() { 
         parent::__construct(); 
         $this->load->database();
        } 
        public function register($enc_password){
            
            $data = array('name' => $this->input->post('name'),'surname' => $this->input->post('surname'),'email' => $this->input->post('email'), 'username' => $this->input->post('username'), 'password' => $enc_password);
            
            $this->db->set($data);
            $this->db->insert('UserAccount',$data);
        }
        
        
        
        public function login($username, $password){
            $query = $this->db->get_where("UserAccount",array("username"=>$username,
                                                             "password"=>md5($password)));
            
            $data['records'] = $query->result();
            return $data['records'];
        }
        
        
        
        
        public function check_username_exists($username){
            $query = $this->db->get_where('UserAccount', array('username' => $username));
            if(empty($query->row_array())){
                return true;
            } else {
                return false;
            }
        }
        public function check_email_exists($email){
            $query = $this->db->get_where('UserAccount', array('email' => $email));
            if(empty($query->row_array())){
                return true;
            } else {
                return false;
            }
        }
    }