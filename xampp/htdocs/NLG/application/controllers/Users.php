<?php
    class Users extends CI_Controller{
        
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
        
        
        public function register(){
            $data['title'] = 'Sign Up';
            
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('username','Username','required|callback_check_username_exists');
            $this->form_validation->set_rules('email','Email','required|callback_check_email_exists');
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_rules('password2','Confirm Password','matches[password]');
            
            if($this->form_validation->run() === FALSE){
                $this->load->view('templates/header');
                $this->load->view('users/register' , $data);
                $this->load->view('templates/footer');
            }else {
                $enc_password = md5($this->input->post('password'));
                $this->user_model->register($enc_password);
                $this->session->set_flashdata('user_registered','You have Signed Up');
                redirect('home');
            }
        }
        
        public function login(){
              $this->load->model('User_model');
            $data['title'] = 'Sign In';
             
            $this->form_validation->set_rules('username','Username','required');
            $this->form_validation->set_rules('password','Password','required');
            
            if($this->form_validation->run() === FALSE){
                $this->load->view('templates/header');
                $this->load->view('users/login' , $data);
                $this->load->view('templates/footer');
            }else {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $result = $this->user_model->login($username,$password);
                //print_r($this->input->post);
                //print_r($result);
                //echo count($result);
                if(count($result)>0){
                    $user_data = array(
                        'user_id' => $result,
                        'username' => $username,
                        'logged_in' => true
                    );
                    $this->session->set_userdata($user_data);
                    $this->session->set_flashdata('user_signedin','You are Signed In');
                     redirect('home');
                } else{
                    $this->session->set_flashdata('signin_failed','Failed To Sign In');
                    redirect('users/login');
                }
                
            }
        }
        
        public function logout(){
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('username');
            $this->session->set_flashdata('user_signedout','You have Signed Out');
            redirect('users/login');
        }
        
        public function check_username_exists($username){
            $this->form_validation->set_message('check_username_exists','That is already taken.');
            if($this->user_model->check_username_exists($username)){
                return true;
            } else{
                return false;
            }
        }
        public function check_email_exists($email){
            $this->form_validation->set_message('check_email_exists','That email already has an account.');
            if($this->user_model->check_email_exists($email)){
                return true;
            } else{
                return false;
            }
        }
        
    }