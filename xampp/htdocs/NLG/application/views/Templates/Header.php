<html>
    <head>
        <title>NLG Music</title>   
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

    </head>
    <style>
        body{
            background-color: #ADD8E6;
        }
  </style>
    <body onload="myFunction()">
    <nav class='navbar navbar-inverse'>
           <div class='navbar-header'>
               
               <a class="navbar-brand" href="/">NLG Music</a>
           </div>
           <div id='navbar'>
               <ul class='nav navbar-nav'>
                   <li><a href="<?php echo base_url(); ?>">Home</a></li>
                   <li><a href="<?php echo base_url(); ?>/about">About</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                   <?php if(!$this->session->userdata('logged_in')):?>
                       <li><a href="<?php echo base_url(); ?>users/register">Sign Up</a></li>
                       <li><a href="<?php echo base_url(); ?>users/login">Sign In</a></li>
                   <?php endif; ?>
                   <?php if($this->session->userdata('logged_in')):?>
                    <li><a href="<?php echo base_url(); ?>users/logout">Sign Out</a></li>
                   <?php endif; ?>
               </ul>
           </div>
    </nav>
        <?php if($this->session->flashdata('user_registered')): ?>
            <?php echo '<p class="alert alert-danger">' .$this->session->flashdata('user_registered').'</p>'?>
        <?php endif; ?>
        <?php if($this->session->flashdata('signin_failed')): ?>
            <?php echo '<p class="alert alert-danger">' .$this->session->flashdata('signin_failed').'</p>'?>
        <?php endif; ?>
        <?php if($this->session->flashdata('user_signedin')): ?>
            <?php echo '<p class="alert alert-danger">' .$this->session->flashdata('user_signedin').'</p>'?>
        <?php endif; ?>
        <?php if($this->session->flashdata('user_signedout')): ?>
            <?php echo '<p class="alert alert-danger">' .$this->session->flashdata('user_signedout').'</p>'?>
        <?php endif; ?>