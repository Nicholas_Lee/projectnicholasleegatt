
<div class="container">
    <h2><?= $title ?></h2>
    <h2>Give Us Feedback or Contact Us</h2>
    <form action="<?=base_url()?>index.php/Feedbackcontact/postEmail" method="POST">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" id="subject" name="subject">
        </div>
        <div class="form-group">
            <label for="message">Message</label>
            <textarea class="form-control" id="message" name="message"></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-danger" type="submit">Send</button>
        </div>
    </form>
</div>