
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
     img {
          width: 100%;
          height: 300px;
     }
     .technoPlay1 {
          display: inline-block;
          padding: 15px 25px;
          font-size: 15px;
          cursor: pointer;
          text-align: center;
          text-decoration: none;
          outline: none;
          color: #fff;
          background-color: #9bc2cf;
          border: none;
          border-radius: 15px;
          box-shadow: 0 9px #999;
     }
    .technoPlay1:hover {background-color: #3e8e41}

    .technoPlay1:active {
          background-color: #9bc2cf;
          box-shadow: 0 5px #666;
          transform: translateY(4px);
    }
    body{
          background-color: #ADD8E6;
    }


    #TechnoHead{
          text-align: center;
    }
    table{
          background-color: aliceblue;
    }
    th{
          font-size: 20px;
    }
    #welcome{
          text-align: center;
          font-size: 30px;
    }
    td{
          font-size: 15px;
    }
    #technoPlay1{
          float: left;
    }
    #wrapper{
        width: 640px;
        margin: 10px auto;
    }
    #audioPlayer{
        background-color: darkgrey;
        width: 640px;
        padding-bottom: 3px;
    }
    #defaultBar{
        background-color: black;
        width: 640px;
        height: 10px;
        position: relative;
    }
    #progressBar{
        background-color: #9bc2cf;
        height: 10px;
        width: 0px;
        position: absolute;
    }
    #playButton{
        background-color: darkgrey;
        border: none;
        height: 32px;
        width: 32px;
        background-image: url(application/img/iconmonstr-media-control-4-32.png);
        background-repeat: no-repeat;
        background-position: center;
        padding-bottom: 4px;
    }
    #muteButton{
        background-color: darkgrey;
        border: none;
        height: 32px;
        width: 32px;
        background-image: url(application/img/iconmonstr-audio-5-32.png);
        background-repeat: no-repeat;
        background-position: center;
        padding-bottom: 3px;
    }
    #muteButton:active,#playButton:active{
        position: relative;
        top: 2px;
    }
    #volume-bar{
        position: relative;
        top: 5px;
        width: 500px;
        float: right;
    }
</style>

<img src="application/img/homeheader.jpg" alt="home" >
<p id="welcome">Welcome to NLG Music</p>
<div class="container">
  <h2 id="TechnoHead">Trance Radio Stations</h2>           
  <table class="table">
    <thead>
      <tr>
        <th>Play</th>
        <th>Station</th>
        <th>BitRate</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        
        <td><button class="technoPlay1" onclick="atgTrance()" ><i class="fa fa-play"></i></button></td>
        <td>DI Radio Digital Impulse - ATG Trance</td>
        <td>320</td>
      </tr>
      <tr>
        <td><button class="technoPlay1" onclick="pulsTrance()" ><i class="fa fa-play"></i></button></td>
        <td>PulsRadio TRANCE</td>
          <td>192</td>
      </tr>
      <tr>
        <td><button class="technoPlay1" onclick="athTrance()" ><i class="fa fa-play"></i></button></td>
        <td>Trance Athens Radio</td>
          <td>192</td>
      </tr>
    </tbody>
  </table>
    <div id="wrapper">
        <audio id="radioLink" >
            <source id="radiostr" src="http://149.202.208.214:8008/stream" type="audio/mp3">
        </audio>
        <nav id='audioPlayer'>
            <div id="defaultBar">
                <div id="progressBar"></div>
            </div>
            <div id="buttons">
                <button type="button" id="playButton" ></button> 
                <button type="button" id="muteButton"></button>
                <input type="range" id="volume-bar" min="0" max="1" step="0.01" value="1">
                <span id="currentTime">0:00</span>
                
            </div>
        </nav>
    </div>    
</div>
<script type="text/javascript">
 var radioLink = document.getElementById('radioLink');
 var playButton = document.getElementById('playButton');
 var muteButton = document.getElementById('muteButton');
 var volumeBar = document.getElementById("volume-bar");
 var currentTime = document.getElementById("currentTime");
 var technoPlay1 = document.getElementById("technoPlay1");
 var technoPlay2 = document.getElementById("technoPlay2");
 var technoPlay3 = document.getElementById("technoPlay3");
    var radiostr = document.getElementById("radiostr");
 playButton.addEventListener('click',playOrPause,false);
 muteButton.addEventListener('click',muteOrUnmute,false);
 volumeBar.addEventListener("change", function() {
  radioLink.volume = volumeBar.value;
});
 function playOrPause(){
     if(!radioLink.paused && !radioLink.ended){
         radioLink.pause();
         playButton.style.backgroundImage = 'url(application/img/iconmonstr-media-control-4-32.png)';
         window.clearInterval(updateTime);
     } else{
         radioLink.play();
         playButton.style.backgroundImage = 'url(application/img/iconmonstr-media-control-8-32.png)';
         updateTime = setInterval(update,500);
     }
     
 }
 function muteOrUnmute(){
     if(radioLink.muted == true){
         radioLink.muted = false;
         muteButton.style.backgroundImage = 'url(application/img/iconmonstr-audio-5-32.png)';
     } else{
         radioLink.muted = true;
         muteButton.style.backgroundImage = 'url(application/img/iconmonstr-audio-9-32.png)';
     }
 }
  function update(){
      if(!radioLink.ended){
          var playedMinutes = parseInt(radioLink.currentTime/60);
          var playedSeconds = parseInt(radioLink.currentTime%60);
          currentTime.innerHTML = playedMinutes + ':' + playedSeconds;
          
      }else{
          currentTime = 0.00;
      }
  }
  function athTrance(){
      radiostr.src = 'http://149.202.208.214:8008/stream';
      if(radioLink.paused){
          playOrPause();
      }else{
          radioLink.play();
      }

  }  
  function atgTrance(){
      radiostr.src = 'http://5.39.71.159:8430/stream';
     if(radioLink.paused){
          playOrPause();
      }else{
          radioLink.play();
      }
  }    
    function pulsTrance(){
      radiostr.src = 'http://193.200.42.211:80/pulstranceHD.mp3';
     if(radioLink.paused){
          playOrPause();
      }else{
          radioLink.play();
      }
  }    
</script>
